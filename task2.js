const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send('Напишите путь"')
});

app.get('/encode/:name', (req, res) => {
    const plainText = req.params.name;
    const keys = 'password';
    res.send(Vigenere.Cipher(keys).crypt(plainText));
});

app.get('/decode/:name', (req, res) => {
    const plainText = req.params.name;
    const keys = 'password';
    res.send(Vigenere.Decipher(keys).crypt(plainText));
});

app.listen(port, () => {
    console.log('server started on ' + port);
})